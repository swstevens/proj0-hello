# Project 0
-----------

Author: Shea Stevens
Contact: sstevens@cs.uoregon.edu

This program uses a credentials file to print Hello World. You can run the program by 
typing ``make run`` in the main repo directory. Changes have been made to the Makefile
so that the program runs properly when this is called.
